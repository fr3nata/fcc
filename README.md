# FCC Projects

## Front-End
### Intermediate

1. Quotes :white_check_mark:
 - [x] [jQuery](http://quotes-js.frenata.net)
 - [x] [Elm](http://quotes-elm.frenata.net)
2. Weather
 - [x] [Elm](http://weather-elm.frenata.net)
3. Wikipedia :white_check_mark:
 - [x] [JS](http://wiki-js.frenata.net)
 - [x] [Elm](http://wiki-elm.frenata.net)
4. Twitch

### Advanced

1. Calculator
2. Pomodoro
3. Tic Tac Toe
4. Simon

## Back-End 
### API Projects

1. Timestamp
 - [x] [Go](https://timestamp-go.herokuapp.com/)
2. Request Headers
 - [x] [Go](https://request-headers-go.herokuapp.com/)
3. URL Shortener
4. Image Search Abstraction
5. File Metadata

### Dynamic Web Apps

1. Voting
2. Nightlife
3. Stock Market
4. Book Club
5. Pinterest
